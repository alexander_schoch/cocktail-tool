FROM node:18 AS js-builder

WORKDIR /app
COPY . .
RUN npm i
RUN npm run build


FROM php:8.2-fpm AS run

RUN mkdir -p /var/www/cocktails
WORKDIR /var/www/cocktails
 
RUN apt update && apt install -y wget curl libzip4 libzip-dev git nginx libxslt1-dev libpng-dev libonig-dev

RUN docker-php-ext-configure zip && \ 
    docker-php-ext-install zip pdo pdo_mysql xsl gd intl opcache mbstring

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
  php composer-setup.php && \
  php -r "unlink('composer-setup.php');" && \
  mv composer.phar /usr/local/bin/composer

COPY composer.json .

RUN composer update
RUN composer install

COPY nginx.conf /etc/nginx/sites-enabled/default

COPY --chown=www-data:www-data . /var/www/cocktails
COPY --from=js-builder /app/public/build /var/www/cocktails/public/build

RUN chown -R www-data:www-data /var/www/cocktails

EXPOSE 80 443

ENTRYPOINT ["bash", "entrypoint.sh"]
