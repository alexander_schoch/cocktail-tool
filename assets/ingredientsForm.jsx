import React from 'react';
import ReactDOM from 'react-dom';
import { useState } from 'react';

import { Row, Col } from 'react-bootstrap';

import { styled } from '@mui/material/styles';
import Alert from '@mui/material/Alert';
import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Snackbar from '@mui/material/Snackbar';
import Switch from '@mui/material/Switch';
import TextField from '@mui/material/TextField';

import axios from 'axios';

class IngredientsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      types: [],
      name: '',
      type: '',
      price: '',
      isPerLiter: true,
      isAvailable: true,
      snackbar: false,
      snackbarError: false,
    };
    this.getTypes = this.getTypes.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.sendToDatabase = this.sendToDatabase.bind(this);
    this.isNewType = this.isNewType.bind(this);

    this.getTypes();
  }

  async getTypes() {
    const response = await fetch('/types-api');
    const data = await response.json();
    const types = data.map((element) => {return element.Name});
    this.setState({
      types: types,
    });
  }

  sendToDatabase() {
    axios.put('/insert/ingredient', {
        Name: this.state.name,
        Type: this.state.type,
        isAvailable: this.state.isAvailable,
        isPerLiter: this.state.isPerLiter,
        Price: this.state.price,
      }).then(response => {
        this.setState({
          name: '',
          type: '',
          price: '',
          isPerLiter: true,
          isAvailable: true,
          snackbar: true,
          snackbarError: false,
        });
      })
  }

  handleSubmit() {
    console.log(this.state);
    if(this.state.name != '' && this.state.type != '' && this.state.price != '') {
      if(this.isNewType()) {
        axios.put('/insert/type', {
          Name: this.state.type,
        }).then(reponse => {
          this.sendToDatabase();
        });
      } else {
        this.sendToDatabase();
      }
      this.getTypes();
    } else {
      this.setState({snackbarError: true});
    }
  }

  isNewType() {
    if(this.state.types.includes(this.state.type)) return false;
    return true;
  }

  render() {
    return (
      <>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField 
              fullWidth
              label="Name" 
              variant="outlined" 
              value={this.state.name}
              onChange={(e) => {this.setState({name: e.target.value});}}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <Autocomplete
              freeSolo
              options={this.state.types}
              onChange={(e, v) => {this.setState({type: v});}}
              value={this.state.type !== null ? this.state.type : ''}  
              onInputChange={(e, v) => {this.setState({type: v});}}
              inputValue={this.state.type !== null ? this.state.type : ''}
              renderInput={(params) => <TextField {...params} label="Ingredient Type" />}
            />
          </Grid>
          <Grid item xs={8} md={6}>
            <TextField 
              fullWidth
              label="Price" 
              variant="outlined" 
              value={this.state.price}
              onChange={(e) => {this.setState({price: e.target.value}); }}
              inputProps={{
                step: "0.1",
                pattern: '[0-9]*'
              }}
            />
          </Grid>
          <Grid item xs={4} md={3}>
            <FormControlLabel control={<Switch onChange={() => {this.setState({isPerLiter: !this.state.isPerLiter});}} checked={this.state.isPerLiter}/>} label={this.state.isPerLiter ? "Per Liter" : "Per Serving"} />
          </Grid>
          <Grid item xs={12} md={3}>
            <FormControlLabel control={<Switch onChange={() => {this.setState({isAvailable: !this.state.isAvailable});}} checked={this.state.isAvailable}/>} label={this.state.isAvailable ? "Available" : "Not Available"} />
          </Grid>
            { 
                this.isNewType() && <Grid item xs={12}><Alert severity="info">The Ingredient Type <b>{this.state.type}</b> will be added to the types database.</Alert></Grid> || <></>
            }
          <Grid item xs={3}>
            <Button variant="contained" onClick={this.handleSubmit}>Submit</Button>
          </Grid>
       </Grid>

       <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
         <Alert severity="success" sx={{ width: '100%' }}>
          The Ingredient was successfully added! 
         </Alert>
       </Snackbar>
       <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
         <Alert severity="error" sx={{ width: '100%' }}>
          Please completely fill out the form
         </Alert>
       </Snackbar>
      </>
    )
  }
}

export default IngredientsForm;
