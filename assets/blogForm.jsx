import React from 'react';
import ReactDOM from 'react-dom';

import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Alert,
  Autocomplete,
  Button,
  Grid,
  Snackbar,
  TextField,
  Typography,
} from '@mui/material';

import { ThemeProvider } from '@mui/material/styles';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import FileUploadIcon from '@mui/icons-material/FileUpload';

import theme from './theme';

import axios from 'axios';

class BlogForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      name: '',
      cocktail: null,
      text: '',
      file: '',
      cocktails: [],
      snackbar: false,
      snackbarError: false,
    };
    this.getCocktails = this.getCocktails.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.getCocktails();
  }

  async getCocktails() {
    const response = await fetch('/cocktails-api');
    const data = await response.json();
    const cocktails = data.map((c) => {return c.Name});
    this.setState({
      cocktails: cocktails,
    });
  }

  handleSubmit() {
    console.log(this.state.cocktail);
    if(this.state.name != '' && this.state.title != '' && this.state.cocktail !== null && this.state.file != '') {
      let formData = new FormData();
      formData.append('file', this.state.file);
      axios.post('/blog-api-image', formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }).then((response) => {
        axios.put('/blog-api-insert', {
          title: this.state.title,
          name: this.state.name,
          cocktail: this.state.cocktail,
          text: this.state.text,
          file: this.state.file.name,
        }).then(response => {
          console.log(response.data);
          this.setState({
            title: '',
            name: '',
            cocktail: '',
            text: '',
            file: '',
            snackbar: true,
            snackbarError: false,
          });
        });
      });
    } else {
      this.setState({snackbarError: true});
    }
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <Accordion style={{ marginTop: '20px' }}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon/>}
            aria-controls="panel1a-content"
          >
            <Typography variant="h5">
              Create Post
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={2}>
              <Grid item xs={12} md={4}>
                <TextField 
                  fullWidth
                  label="Title" 
                  variant="outlined" 
                  value={this.state.title}
                  onChange={(e) => this.setState({title: e.target.value})}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <TextField 
                  fullWidth
                  label="Name" 
                  variant="outlined" 
                  value={this.state.name}
                  onChange={(e) => this.setState({name: e.target.value})}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <Autocomplete
                  fullWidth
                  disablePortal
                  options={this.state.cocktails}
                  onChange={(e, v) => this.setState({cocktail: v})}
                  renderInput={(params) => <TextField {...params} label="Cocktail" />}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField 
                  fullWidth
                  label="Text" 
                  variant="outlined" 
                  value={this.state.text}
                  multiline
                  onChange={(e) => this.setState({text: e.target.value})}
                />
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  component="label"
                  startIcon={<FileUploadIcon/>}
                >
                  <input name="upload-photo" type="file" accept="image/png, image/jpeg" hidden onChange={(e) => this.setState({file: e.target.files[0]})}/>
                  Upload Photo
                </Button>
                {this.state.file != '' && 
                  <Typography component="span">  Selected: {this.state.file.name}</Typography>
                }
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  onClick={() => this.handleSubmit()}
                >
                  Submit
                </Button>
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion>
        <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
          <Alert severity="success" sx={{ width: '100%' }}>
           The Ingredient was successfully added! 
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
          <Alert severity="error" sx={{ width: '100%' }}>
           Please completely fill out the form
          </Alert>
        </Snackbar>
      </ThemeProvider>
    )
  }
}

export default BlogForm;
