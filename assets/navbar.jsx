import React from 'react';
import ReactDOM from 'react-dom';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import Drawer from '@mui/material/Drawer';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';

import LiquorIcon from '@mui/icons-material/Liquor';
import LocalBarIcon from '@mui/icons-material/LocalBar';
import CategoryIcon from '@mui/icons-material/Category';
import CameraIcon from '@mui/icons-material/Camera';
import MenuIcon from '@mui/icons-material/Menu';

import {ThemeProvider} from '@mui/material/styles';
import { styled } from '@mui/material/styles';
import theme from './theme';

class Navbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      drawer: false,
    };

    this.handleOpenNavMenu = this.handleOpenNavMenu.bind(this);
    this.handleCloseNavMenu = this.handleCloseNavMenu.bind(this);
  }

  handleOpenNavMenu = () => {
    this.setState({drawer: true});
  };

  handleCloseNavMenu = () => {
    this.setState({drawer: false});
  };

  render () {
    const targets = [
      ['Cocktails', '/', <LocalBarIcon/>],
      ['Ingredients', '/ingredients', <LiquorIcon/>],
      ['Ingredient Types', '/types', <CategoryIcon/>],
      ['Blog', '/blog', <CameraIcon/>]
    ]
    return (
      <Box>
      <ThemeProvider theme={theme}>
      <AppBar position="sticky" style={{ backgroundColor: theme.palette.primary.dark }} elevation={0}>
        <Container>
          <Toolbar disableGutters>
            <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
              <IconButton
                size="large"
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={this.handleOpenNavMenu}
                color="inherit"
              >
                <MenuIcon />
              </IconButton>
              <Drawer
                anchor='left'
                open={this.state.drawer}
                onClose={this.handleCloseNavMenu}
              >
                <List>
                  {
                    targets.map(t => (
                      <ListItem button key={t[0]}>
                        <ListItemIcon>
                          {t[2]}
                        </ListItemIcon>
                        <ListItemText primary={t[0]} onClick={() => {window.location = t[1];}} />
                      </ListItem>
                    ))
                  }
                </List>
              </Drawer>
            </Box>
            { 
              // Desktop 
            }
            <a href="/">
              <img src="logo.svg" style={{ maxWidth: '150px' }}/>
            </a>
            <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, marginLeft: '30px' }}>
              {
                targets.map(t => (
                  <Button
                    key={t[0]}
                    onClick={() => {window.location = t[1];}}
                    sx={{ my: 2, color: 'white', display: 'block' }}
                  >
                    {t[0]}
                  </Button>
                ))
              }
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      </ThemeProvider>
      </Box>
    );
  }
}

ReactDOM.render(<Navbar/>, document.getElementById('navbar'));
