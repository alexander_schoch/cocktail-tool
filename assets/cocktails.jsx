import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';

import Carousel from 'react-bootstrap/Carousel';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import Paper from '@mui/material/Paper';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Switch from '@mui/material/Switch';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import {green} from '@mui/material/colors';
import {red} from '@mui/material/colors';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AddIcon from '@mui/icons-material/Add';

import { ThemeProvider } from '@mui/material/styles';
import { useMediaQuery } from "@mui/material";
import { styled } from '@mui/material/styles';

import Masonry from '@mui/lab/Masonry';

import CocktailsForm from './cocktailsForm';
import theme from './theme';
import HowAbout from './howAbout';

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    color: theme.palette.text.secondary,
    border: '1px solid black',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
}));

class Cocktails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cocktails: [],
      types: [],
      randomCocktail: undefined,
    }

    this.getData = this.getData.bind(this);
    this.getTypes = this.getTypes.bind(this);
    this.getLowestPrice = this.getLowestPrice.bind(this);
    this.cocktailItem = this.cocktailItem.bind(this);
		this.getIngredientString = this.getIngredientString.bind(this);
  }

  componentDidMount() {
    this.interval = setInterval(this.getTypes, 6000);
    this.interval = setInterval(this.getData, 3000);
    this.getTypes();
    this.getData();
  }

  async getData() {
    const response = await fetch('/cocktails-api');
    const data = await response.json();
    this.setState({
      cocktails: data,
    });
    if(this.state.randomCocktail === undefined) {
      let index = Math.floor((Math.random() * this.state.cocktails.length));
      while(this.state.cocktails[index].Price.length == 0) {
        index = Math.floor((Math.random() * this.state.cocktails.length));
      }
      this.setState({
        randomCocktail: this.state.cocktails[index],
      });
    }
  }

  async getTypes() {
    const response = await fetch('/types-api');
    const data = await response.json();
    this.setState({
      types: data,
    });
  }

  getLowestPrice(prices) {
    let lowest = prices[0][0];
    prices.map((p) => {
      if(p[0] < lowest)
        lowest = p[0];
    });
    return lowest;
  }

	getIngredientString(recipe) {
		let str = '';
		recipe.map(r => {
			str += (this.state.types.length == 0 ? '(loading...)' : this.state.types.filter(type=>type.id == r.type)[0].Name) + ', ';
		});
		return str.substring(0, str.length-2);
	}

  cocktailItem(c) {
    return (
			<Card elevation={3}>
				<CardContent>
          <Typography variant="h5" color={(c.Price.length == 0) ? "text.secondary" : "primary.main"} component="p">
            {c.Name}
          </Typography>
				 	<Typography variant="h6" color={(c.Price.length == 0) ? "text.secondary" : "primary.light"} component="p" style={{ fontSize: '13pt' }}>
            {this.getIngredientString(c.Recipe)}
          </Typography>
					<Typography style={{ marginTop: '10px' }} color={(c.Price.length == 0) ? "text.secondary" : "text.primary"}>
						{c.Description}
					</Typography>

					{c.Price.length > 0 ?
					<Box style={{ marginTop: '20px' }}>
						<Accordion>
							<AccordionSummary
								expandIcon={<ExpandMoreIcon/>}
								aria-controls="panel1a-content"
							> 
								<Typography variant="h6">{this.getLowestPrice(c.Price)} CHF</Typography>
							</AccordionSummary>
							<AccordionDetails>
								{c.Price.map((p) => (
									<Typography key={p[1]} style={{ paddingTop: '10px' }}>
										{p[0]} CHF: {p[1]}
									</Typography>
								))}
							</AccordionDetails>
						</Accordion> 

						<Accordion>
							<AccordionSummary
								expandIcon={<ExpandMoreIcon/>}
								aria-controls="panel1a-content"
							>
								<Typography variant="h6">
									Recipe
								</Typography>
							</AccordionSummary>
							<AccordionDetails>
								{c.Recipe.map((r) => (
									<Typography key={r.type}>
										{r.amount} {r.unit} {(this.state.types.length == 0 ? '(loading...)' : this.state.types.filter(type=>type.id == r.type)[0].Name)}
									</Typography>
								))}
							</AccordionDetails>
						</Accordion>
					</Box> :
					<Box style={{ marginTop: '20px' }}>
						<Accordion>
							<AccordionSummary
								expandIcon={<ExpandMoreIcon/>}
								aria-controls="panel1a-content"
							>
								<Typography variant="h6" color="text.secondary">
									Recipe
								</Typography>
							</AccordionSummary>
							<AccordionDetails>
								{c.Recipe.map((r) => (
									<Typography key={r.type} color="text.secondary">
										{r.amount} {r.unit} {(this.state.types.length == 0 ? '(loading...)' : this.state.types.filter(type=>type.id == r.type)[0].Name)}
									</Typography>
								))}
							</AccordionDetails>
						</Accordion>
					</Box>
					}

				</CardContent>
			</Card>
		);
  }

  render () { 
    return (
      <>
      <ThemeProvider theme={theme}>
        <Box style={{ backgroundColor: theme.palette.primary.dark, paddingTop: '50px', paddingBottom: '50px', color: '#ffffff' }}>
          <Container>
            <HowAbout c={this.state.randomCocktail} types={this.state.types}/>
          </Container>
        </Box>
      </ThemeProvider>

      <Container style={{ marginTop: '20px' }}>
        <ThemeProvider theme={theme}>
        <Accordion elevation={3}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon/>}
            aria-controls="panel1a-content"
          >
            <AddIcon style={{ marginRight: '10px' }}/> 
            <Typography variant="h5">
              Add new Cocktail
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <CocktailsForm/>
          </AccordionDetails>
        </Accordion>

        <Box style={{ marginTop: '46px', marginRight: '-16px'}}>
         <Masonry columns={{xs: 1, sm: 2, md:3}} spacing={2}>
            {this.state.cocktails.map((c) => {
              return c.Price.length > 0 && 
                <Box key={c.id}>
                  {this.cocktailItem(c)}
                </Box>}
            )}
          </Masonry>
        </Box>

        <Box style={{ marginTop: '20px', marginRight: '-16px'}}>
          <Masonry columns={{xs: 1, sm: 2, md:3}} spacing={2}>
            {this.state.cocktails.map((c) => {
              return c.Price.length == 0 &&
                <Box key={c.id}>
                  {this.cocktailItem(c)}
                </Box>}
            )}
          </Masonry>
        </Box>
 
        </ThemeProvider> 
      </Container>
      </>
    )
  }
}

ReactDOM.render(<Cocktails/>, document.getElementById('main'));
