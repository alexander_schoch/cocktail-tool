import React from 'react';

import {ThemeProvider} from '@mui/material/styles';
import {Typography} from '@mui/material';

import theme from './theme';

const HowAbout = (props) => {
  if(props.c !== undefined && props.types.length > 0) {
    const c = props.c;
    let ingredients = '';
    c.Recipe.map((r, i) => {
      const delimiter = (i < c.Recipe.length - 2) ? ', ' : ' and ';
      ingredients += props.types.filter(type=>type.id == r.type)[0].Name + delimiter;
    });
    return (
      <ThemeProvider theme={theme}>
        <Typography variant="h4">
          How about a{['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'].indexOf(c.Name.charAt(0)) ? '' : 'n'} <Typography component="span" variant="h4" color="primary.light">{c.Name}</Typography> containing <Typography component="span" variant="h4" color="primary.light">{ingredients.substring(0, ingredients.length-5)}</Typography>?
        </Typography>
      </ThemeProvider>
    )
  } else return <></>;
}

export default HowAbout;
