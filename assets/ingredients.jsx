import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Switch from '@mui/material/Switch';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import {green} from '@mui/material/colors';
import {red} from '@mui/material/colors';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

import { ThemeProvider } from '@mui/material/styles';
import { useMediaQuery } from "@mui/material";

import Masonry from '@mui/lab/Masonry';

import IngredientsForm from './ingredientsForm';
import fonts from './theme';

class Ingredients extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ingredients: [],
    }

    this.getData = this.getData.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    axios.put('/ingredients-availability-api/' + e.target.value, {}).then(response => {
      this.getData();
    });
  }

  componentDidMount() {
    this.interval = setInterval(this.getData, 3000);
    this.interval = setInterval(this.getData, 3000);
    this.getData();
  }

  async getData() {
    const response = await fetch('/ingredients-api');
    const data = await response.json();
    this.setState({
      ingredients: data,
    });
  }

  render () { 
    // style={{ fontWeight: 'bold', background: '-webkit-linear-gradient(60deg, ' + (ing.isAvailable ? green[900] : red[900]) + ', ' + (ing.isAvailable ? green[100] : red[100]) + ')', WebkitBackgroundClip: "text", WebkitTextFillColor: "transparent"}}
    return (
      <Container>
        <ThemeProvider theme={fonts}>
          <Accordion elevation={6} style={{ marginTop: '20px' }}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon/>}
              aria-controls="panel1a-content"
            >
              <Typography>
                Add new Ingredient
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <IngredientsForm/>
            </AccordionDetails>
          </Accordion>

          <Box style={{ marginTop: '20px', marginRight: (window.innerWidth < 600 ? '0px' : '-16px') }}>
            <Masonry columns={{xs: 1, sm: 2, md: 3}} spacing={2}>
              {this.state.ingredients.map((ing) => (
                <Card key={ing.id} sx={{ minWidth: 275 }} elevation={6} style={{
                  minWidth: (window.innerWidth < 600 ? '100%' : '0%'),
                }}>
                  <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      {ing.IngredientType.name}
                    </Typography>
                    <Grid container spacing={0}>
                      <Grid item xs={8}>
                        <Typography variant="h5" component="div">
                          {ing.Name}
                        </Typography>
                      </Grid>
                      <Grid item xs={4} style={{ textAlign: 'right' }}>
                        <Switch 
                          value={ing.id}
                          checked={ing.isAvailable}
                          onChange={this.handleChange}
                        />
                      </Grid>
                    </Grid>
                    <Typography sx={{ mb: 1.5 }} color="text.secondary">
                      {ing.Price} CHF per {ing.isPerLiter ? "Liter" : "Serving" }
                    </Typography>
                  </CardContent>
                </Card>
              ))}
            </Masonry>
          </Box>
        </ThemeProvider> 
      </Container>
    )
  }
}

ReactDOM.render(<Ingredients/>, document.getElementById('main'));
