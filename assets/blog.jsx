import React from 'react';
import ReactDOM from 'react-dom';

import {
  Box,
  Card,
  CardContent,
  Typography,
} from '@mui/material';

import {Masonry} from '@mui/lab';

import { ThemeProvider } from '@mui/material/styles';

import theme from './theme';

import { 
  Container
} from '@mui/material';

import BlogForm from './blogForm';

class Blog extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
    }

    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.interval = setInterval(this.getData, 6000);
    this.getData();
  }

  async getData() {
    const response = await fetch('/blog-api');
    const data = await response.json();
    this.setState({
      posts: data,
    });
  }

  render () { 
    return (
      <Container>
        <BlogForm/>

        <ThemeProvider theme={theme}>
          <Box style={{ marginTop: '20px', marginRight: '-16px' }}>
            <Masonry columns={{sx: 1, sm: 2, md: 3}} spacing={2}>
              {this.state.posts.map((post) => (
                <Card key={post.timestamp} elevation={6}>
                  <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      {post.timestamp}
                    </Typography>
                    <Typography variant="h5" gutterBottom>
                      {post.title}
                    </Typography>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      by {post.name}, enjoying a <Typography component="span" style={{ color: theme.palette.primary.main }}>{post.cocktail}</Typography>
                    </Typography>
                    <Typography>
                      {post.text}
                    </Typography>
                    <img
                      style={{ width: '100%', borderRadius: '5px', marginTop: '10px' }}
                      src={"/media/" + post.filename}
                    />
                  </CardContent>
                </Card>
              ))}
            </Masonry>
          </Box>
        </ThemeProvider>
      </Container>
    )
  }
}

ReactDOM.render(<Blog/>, document.getElementById('main'));
