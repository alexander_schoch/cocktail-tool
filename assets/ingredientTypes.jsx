import React from 'react';
import ReactDOM from 'react-dom';

import axios from 'axios';

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';

import { ThemeProvider } from '@mui/material/styles';
import { useMediaQuery } from "@mui/material";

import Masonry from '@mui/lab/Masonry';

import theme from './theme';

const ParseIngredients = (props) => {
  return (
    <ThemeProvider theme={theme}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell><Typography style={{ fontWeight: 'bold' }}>Ingredient</Typography></TableCell>
            <TableCell><Typography style={{ fontWeight: 'bold' }}>Price</Typography></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {props.ingr.map((i) => {
          return i.isAvailable &&
          <TableRow key={i.id}>
            <TableCell>
              <Typography>{i.Name}</Typography>
            </TableCell>
            <TableCell>
              <Typography>{i.Price} CHF per {(i.isPerLiter ? 'Liter' : 'Serving')}</Typography>
            </TableCell>
          </TableRow>
        })}
        </TableBody>
      </Table>
    </ThemeProvider>
  );
}

class IngredientTypes extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      types: [],
    }

    this.getData = this.getData.bind(this);
  }

  componentDidMount() {
    this.interval = setInterval(this.getData, 3000);
    this.getData();
  }

  async getData() {
    const response = await fetch('/types-api');
    const data = await response.json();
    this.setState({
      types: data,
    });
  }

  render () { 
    return (
      <Container>
        <ThemeProvider theme={theme}>
          <Box style={{ marginTop: '20px' }}>
            <Masonry columns={{xs: 1, sm: 2, md: 3}} spacing={2}>
              {this.state.types.map((t) => (
                <Card key={t.id} sx={{ minWidth: 275 }} elevation={6} style={{
                  minWidth: (window.innerWidth < 600 ? '100%' : '0%'),
                }}>
                  <CardContent>
                    <Typography variant="h5">
                      {t.Name}
                    </Typography>
                    <Accordion style={{ marginTop: '20px' }}>
                      <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                      >
                        <Typography variant="h6">      
                          What I currently have 
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <ParseIngredients ingr={t.Ingredients}/>
                      </AccordionDetails>
                    </Accordion>
                    <Accordion>
                      <AccordionSummary
                          expandIcon={<ExpandMoreIcon />}
                          aria-controls="panel1a-content"
                      >
                        <Typography variant="h6">
                          Cocktails using {t.Name}
                        </Typography>
                      </AccordionSummary>
                      <AccordionDetails>
                        <Typography>
                          {t.Cocktails}
                        </Typography>
                      </AccordionDetails>
                    </Accordion>
                  </CardContent>
                </Card>
              ))}
            </Masonry>
          </Box>
        </ThemeProvider> 
      </Container>
    )
  }
}

ReactDOM.render(<IngredientTypes/>, document.getElementById('main'));
