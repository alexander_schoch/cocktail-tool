import {createTheme, ThemeProvider} from '@mui/material/styles';

const fonts = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
});

const theme = createTheme({
  typography: {
    fontFamily: [
      'cmu',
    ].join(','),
  },
  palette: {
    type: 'light',
    primary: {
      main: '#1b5e20',
      light: '#4caf50',
      contrastText: '#c9c0a5',
      dark: '#0d2d0f',
    },
    secondary: {
      main: '#b71c1c',
      light: '#f7c870',
      dark: '#560c0c',
    },
  },
});

export default theme;
