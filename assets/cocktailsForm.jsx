import React from 'react';
import ReactDOM from 'react-dom';
import { useState } from 'react';

import { Row, Col } from 'react-bootstrap';

import { styled } from '@mui/material/styles';
import Alert from '@mui/material/Alert';
import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Snackbar from '@mui/material/Snackbar';
import Switch from '@mui/material/Switch';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

import AddIcon from '@mui/icons-material/Add';

import axios from 'axios';

class CocktailsForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      notes: '',
      types: [],
      recipe: [{
        amount: '',
        unit: '',
        type: '',
      },],
      snackbarError: false,
      snackbar: false,
    };
    this.getTypes = this.getTypes.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.sendToDatabase = this.sendToDatabase.bind(this);
    this.isNewType = this.isNewType.bind(this);

    this.handleAmount = this.handleAmount.bind(this);
    this.handleUnit = this.handleUnit.bind(this);
    this.handleType = this.handleType.bind(this);
    this.addLine = this.addLine.bind(this);
  }

  async getTypes() {
    const response = await fetch('/types-api');
    const data = await response.json();
    const types = data.map((element) => {return element.Name});
    this.setState({
      types: types,
    });
  }

  componentDidMount() {
    this.getTypes();
  }

  handleAmount(data) {
    let rec = this.state.recipe;
    rec[data.field].amount = data.value;
    this.setState({recipe: rec});
  }

  handleUnit (data) {
    let rec = this.state.recipe;
    rec[data.field].unit = data.value;
    this.setState({recipe: rec});
  }

  handleType (data) {
    let rec = this.state.recipe;
    rec[data.field].type = data.value;
    this.setState({recipe: rec});
  }

  addLine() {
    let r = this.state.recipe;
    r.push({amount: '', unit: '', type: ''});
    this.setState({recipe: r});
  }

  sendToDatabase() {
    axios.put('/insert/cocktail', {
      Name: this.state.name,
      Notes: this.state.notes,
      Recipe: this.state.recipe,
    }).then(response => {
      this.setState({
        name: '',
        notes: '',
        recipe: [{
          amount: '',
          unit: '',
          type: '',
        },],
      });
    })
  }

  async handleSubmit() {
    if(this.state.name != '' && this.state.recipe[0].amount != '' && this.state.recipe[0].type != '') {
      const promises = this.state.recipe.map(r => {
        if(this.isNewType(r.type)) {
          axios.put('/insert/type', {
            Name: r.type,
          });
        }
      });
      await Promise.all(promises);
      this.sendToDatabase();
      this.getTypes();
      this.setState({snackbar: true});
    } else {
      this.setState({snackbarError: true});
    }
  }

  isNewType(t) {
    if(this.state.types.includes(t)) return false;
    return true;
  }

  render() {
    return (
      <>
        <Grid container spacing={2}>
          <Grid item xs={12} md={6}>
            <TextField 
              fullWidth
              label="Name" 
              variant="outlined" 
              value={this.state.name}
              onChange={(e) => {this.setState({name: e.target.value});}}
            />
          </Grid>
          <Grid item xs={12} md={6}>
            <TextField 
              multiline
              fullWidth
              label="Notes" 
              variant="outlined" 
              value={this.state.notes}
              onChange={(e) => {this.setState({notes: e.target.value}); }}
            />
          </Grid>
          <Grid item md={12}>
          {this.state.recipe.map((row, i) => (
            <div style={{ marginBottom: '10px', borderTop: '1px dashed grey', marginTop: '20px', paddingTop: '20px' }} key={i}>
            <Grid container spacing={2}>
              <Grid item xs={8} md={4}>
                <TextField 
                  fullWidth
                  label="Amount" 
                  variant="outlined" 
                  value={this.state.recipe[i].amount}
                  onChange={(e) => {this.handleAmount({value: e.target.value, field: i}); }}
                  inputProps={{
                    type: 'number'
                  }}
                />
              </Grid>
              <Grid item xs={4} md={4}>
                <TextField 
                  fullWidth
                  label="Unit" 
                  variant="outlined" 
                  value={this.state.recipe[i].unit}
                  onChange={(e) => {this.handleUnit({value: e.target.value, field: i}); }}
                />
              </Grid>
              <Grid item xs={12} md={4}>
                <Autocomplete
                  freeSolo
                  options={this.state.types}
                  onChange={(e, v) => {this.handleType({value: v, field: i});}}
                  value={this.state.recipe[i].type !== null ? this.state.recipe[i].type : ''}  
                  onInputChange={(e, v) => {this.handleType({value: v, field: i});}}
                  inputValue={this.state.recipe[i].type !== null ? this.state.recipe[i].type : ''}
                  renderInput={(params) => <TextField {...params} label="Ingredient Type" />}
                />
              </Grid>
            </Grid>
            </div>
          ))}
          </Grid>
          <Grid item xs={6}>
            <Button variant="contained" onClick={this.addLine} startIcon={<AddIcon/>}>Add Line</Button>
          </Grid>
          <Grid item xs={6} style={{ textAlign: 'right' }}>
            <Button variant="contained" onClick={this.handleSubmit}>Submit</Button>
          </Grid>
        </Grid>

        <Snackbar open={this.state.snackbar} autoHideDuration={6000} onClose={() => {this.setState({snackbar: false});}}>
          <Alert severity="success" sx={{ width: '100%' }}>
           The Ingredient was successfully added! 
          </Alert>
        </Snackbar>
        <Snackbar open={this.state.snackbarError} autoHideDuration={6000} onClose={() => {this.setState({snackbarError: false});}}>
          <Alert severity="error" sx={{ width: '100%' }}>
           Please completely fill out the form
          </Alert>
        </Snackbar>
      </>
    )
  }
}

export default CocktailsForm;
