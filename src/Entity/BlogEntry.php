<?php

namespace App\Entity;

use App\Repository\BlogEntryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BlogEntryRepository::class)]
class BlogEntry
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $Title;

    #[ORM\Column(type: 'string', length: 255)]
    private $Name;

    #[ORM\ManyToOne(targetEntity: Cocktail::class, inversedBy: 'blogEntries')]
    private $Cocktail;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Text;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $Filename;

    #[ORM\Column(type: 'text')]
    private $Timestamp;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getCocktail(): ?Cocktail
    {
        return $this->Cocktail;
    }

    public function setCocktail(?Cocktail $Cocktail): self
    {
        $this->Cocktail = $Cocktail;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->Text;
    }

    public function setText(?string $Text): self
    {
        $this->Text = $Text;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->Filename;
    }

    public function setFilename(?string $Filename): self
    {
        $this->Filename = $Filename;

        return $this;
    }

    public function getTimestamp(): ?string
    {
        return $this->Timestamp;
    }

    public function setTimestamp(string $Timestamp): self
    {
        $this->Timestamp = $Timestamp;

        return $this;
    }
}
