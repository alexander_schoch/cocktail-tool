<?php

namespace App\Entity;

use App\Repository\CocktailRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CocktailRepository::class)]
class Cocktail
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $Name;

    #[ORM\Column(type: 'text', nullable: true)]
    private $Description;

    #[ORM\Column(type: 'json')]
    private $Recipe = [];

    #[ORM\OneToMany(mappedBy: 'Cocktail', targetEntity: BlogEntry::class)]
    private $blogEntries;

    public function __construct()
    {
        $this->blogEntries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->Description;
    }

    public function setDescription(?string $Description): self
    {
        $this->Description = $Description;

        return $this;
    }

    public function getRecipe(): ?array
    {
        return $this->Recipe;
    }

    public function setRecipe(array $Recipe): self
    {
        $this->Recipe = $Recipe;

        return $this;
    }

    /**
     * @return Collection|BlogEntry[]
     */
    public function getBlogEntries(): Collection
    {
        return $this->blogEntries;
    }

    public function addBlogEntry(BlogEntry $blogEntry): self
    {
        if (!$this->blogEntries->contains($blogEntry)) {
            $this->blogEntries[] = $blogEntry;
            $blogEntry->setCocktail($this);
        }

        return $this;
    }

    public function removeBlogEntry(BlogEntry $blogEntry): self
    {
        if ($this->blogEntries->removeElement($blogEntry)) {
            // set the owning side to null (unless already changed)
            if ($blogEntry->getCocktail() === $this) {
                $blogEntry->setCocktail(null);
            }
        }

        return $this;
    }
}
