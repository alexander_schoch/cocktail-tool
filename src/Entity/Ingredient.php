<?php

namespace App\Entity;

use App\Repository\IngredientRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: IngredientRepository::class)]
class Ingredient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $Name;

    #[ORM\ManyToOne(targetEntity: IngredientType::class, inversedBy: 'ingredients')]
    #[ORM\JoinColumn(nullable: false)]
    private $IngredientType;

    #[ORM\Column(type: 'boolean')]
    private $isAvailable;

    #[ORM\Column(type: 'float')]
    private $Price;

    #[ORM\Column(type: 'boolean')]
    private $isPerLiter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getIngredientType(): ?IngredientType
    {
        return $this->IngredientType;
    }

    public function setIngredientType(?IngredientType $IngredientType): self
    {
        $this->IngredientType = $IngredientType;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->isAvailable;
    }

    public function setIsAvailable(bool $isAvailable): self
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->Price;
    }

    public function setPrice(float $Price): self
    {
        $this->Price = $Price;

        return $this;
    }

    public function getIsPerLiter(): ?bool
    {
        return $this->isPerLiter;
    }

    public function setIsPerLiter(bool $isPerLiter): self
    {
        $this->isPerLiter = $isPerLiter;

        return $this;
    }
}
