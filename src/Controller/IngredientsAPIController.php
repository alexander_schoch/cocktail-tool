<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Repository\IngredientTypeRepository;
use App\Repository\IngredientRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class IngredientsAPIController extends AbstractController
{
    #[Route('/insert/ingredient', name: 'ingredients_api_insert')]
    public function index(Request $request, ManagerRegistry $doctrine, IngredientTypeRepository $itr): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $type = $itr->findOneByName($data->Type);

        $i = new Ingredient();
        $i->setName($data->Name);
        $i->setIngredientType($type);
        $i->setIsAvailable($data->isAvailable);
        $i->setIsPerLiter($data->isPerLiter);
        $i->setPrice($data->Price);

        $em = $doctrine->getManager();
        $em->persist($i);
        $em->flush();

        return new Response('success');
    }

    #[Route('ingredients-api', name: 'ingredients_api')]
    public function ingredients_api(IngredientRepository $ir): JsonResponse
    {
        $data = $ir->findAll();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        
        $json = $serializer->normalize($data, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    #[Route('ingredients-availability-api/{id}', name: 'ingredients_availability_api')]
    public function ingredients_availability_api(ManagerRegistry $doctrine, IngredientRepository $ir, int $id): Response
    {
        $i = $ir->findOneById($id);
        $i->setIsAvailable(!$i->getIsAvailable());

        $em = $doctrine->getManager();
        $em->persist($i);
        $em->flush();

        return new Response('success');
    }
}
