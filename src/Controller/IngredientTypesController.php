<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IngredientTypesController extends AbstractController
{
    #[Route('/types', name: 'ingredient_types')]
    public function index(): Response
    {
        return $this->render('types/index.html.twig');
    }
}
