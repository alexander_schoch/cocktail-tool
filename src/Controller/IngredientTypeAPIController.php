<?php

namespace App\Controller;

use App\Entity\IngredientType;
use App\Repository\IngredientTypeRepository;
use App\Repository\CocktailRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class IngredientTypeAPIController extends AbstractController
{
    #[Route('/types-api', name: 'ingredient_type_api')]
    public function index(IngredientTypeRepository $itr, CocktailRepository $cr): Response
    {
        $data = $itr->findAll(); 
        $cocktails = $cr->findAll();

        $types = [];

        foreach($data as $type) {
          $cocktails_str = '';
          foreach($cocktails as $cocktail) {
            $recipe = $cocktail->getRecipe();
            foreach($recipe as $row) {
              if($row['type'] == $type->getId()) {
                $cocktails_str = $cocktails_str . $cocktail->getName() . ', ';
                break;
              }
            }
          }
          array_push(
            $types,
            [
              'id' => $type->getId(),
              'Name' => $type->getName(),
              'Ingredients' => $type->getIngredients(),
              'Description' => $type->getDescription(),
              'Cocktails' => substr($cocktails_str, 0, -2),
            ]
          );
        }


        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($types, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new JsonResponse($json);
    }

    #[Route('/insert/type', name: 'ingredient_type_api_insert')]
    public function insert_type(Request $request, ManagerRegistry $doctrine): Response
    {
      $data = $request->getContent();
      $data = json_decode($data);

      $t = new IngredientType();
      $t->setName($data->Name);

      $em = $doctrine->getManager();
      $em->persist($t);
      $em->flush();

      return new Response('success');
    }
}
