<?php

namespace App\Controller;

use App\Entity\BlogEntry;
use App\Repository\BlogEntryRepository;
use App\Repository\CocktailRepository;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class BlogAPIController extends AbstractController
{
    #[Route('/blog-api-insert', name: 'blog_api_insert')]
    public function index(Request $request, ManagerRegistry $doctrine, CocktailRepository $cr): Response
    {
        $data = $request->getContent();
        $data = json_decode($data);

        $timestamp = new \DateTimeImmutable();
        $timestamp_str = $timestamp->format('Y-m-d_H-i-s');
        $exp = explode('.', $data->file);
        $ext = $exp[count($exp)-1];
        $filename = $timestamp_str . '_' . str_replace(' ', '_', $data->name) . '.' . $ext;
        rename('media/' . $data->file, 'media/' . $filename); 

        $c = $cr->findOneByName($data->cocktail);

        $blogEntry = new BlogEntry();
        $blogEntry->setTitle($data->title);
        $blogEntry->setName($data->name);
        $blogEntry->setCocktail($c);
        $blogEntry->setText($data->text);
        $blogEntry->setFilename($filename);
        $blogEntry->setTimestamp($timestamp_str);

        $em = $doctrine->getManager();
        $em->persist($blogEntry);
        $em->flush();

        return new Response('OK');
    }

    #[Route('/blog-api-image', name: 'blog_api_image')]
    public function blog_api_image(Request $request): Response
    {
        $data = $request->files->get('file');

        $data->move(
          'media',
          $data->getClientOriginalName()
        );
        
        return new Response('OK');
    }

    #[Route('/blog-api', name: 'blog_api')]
    public function blog_api(BlogEntryRepository $ber): JsonResponse
    {
        $data = $ber->findAll();

        $formatted_data = [];

        foreach($data as $entry) {
          $timestamp = \DateTimeImmutable::createFromFormat('Y-m-d_H-i-s', $entry->getTimestamp())->setTimezone(new \DateTimeZone('CET'));

          array_push(
            $formatted_data,
            [
              'title' => $entry->getTitle(),
              'name' => $entry->getName(),
              'cocktail' => $entry->getCocktail()->getName(),
              'text' => $entry->getText(),
              'timestamp' => $timestamp->format('F jS, Y H:i:s'),
              'filename' => $entry->getFilename(),
            ]
          );
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $json = $serializer->normalize($formatted_data, 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]);

        return new JsonResponse($json);
    }
}
