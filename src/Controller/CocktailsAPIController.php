<?php

namespace App\Controller;

use App\Entity\Cocktail;
use App\Entity\Ingredient;
use App\Repository\IngredientTypeRepository;
use App\Repository\CocktailRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CocktailsAPIController extends AbstractController
{
    #[Route('/insert/cocktail', name: 'cocktail_api_insert')]
    public function index(Request $request, ManagerRegistry $doctrine, IngredientTypeRepository $itr): Response
    {
        //$data = '{"Name":"theName","Notes":"theNotes","Recipe":[{"amount":"10","unit":"cL","type":"Lemons"},{"amount":"","unit":"","type":""}]}';
        $data = $request->getContent();
        $data = json_decode($data);

        $recipe = [];

        foreach($data->Recipe as $row) {
            if($row->amount == '' || $row->type == '')
                continue;
            $type = $itr->findOneByName($row->type);
            array_push(
              $recipe,
              [
                'amount' => $row->amount,
                'unit' => $row->unit,
                'type' => $type->getId(),
              ]
            );
        }

        $c = new Cocktail();
        $c->setName($data->Name);
        $c->setDescription($data->Notes);
        $c->setRecipe($recipe);

        $em = $doctrine->getManager();
        $em->persist($c);
        $em->flush();

        return new Response('success');
    } 

    #[Route('cocktails-api', name: 'cocktails_api')]
    public function cocktails_api(CocktailRepository $cr, IngredientTypeRepository $itr): JsonResponse
    {
        $data = $cr->findAll();

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);

        $cocktails = [];

        foreach($data as $key => $cocktail) {
          $combinations = [];
          $options = [];
          foreach($cocktail->getRecipe() as $row) {
            $type = $itr->findOneById($row['type']);
            $ingredients = $type->getIngredients()->getValues();
            $ing_avail = [];
            foreach($ingredients as $ingredient) {
              if($ingredient->getIsAvailable())
                array_push(
                  $ing_avail,
                  $ingredient
                );
            }
            array_push(
              $options,
              $ing_avail,
            );
          }
          $combinations = $this->getCombinations($options);
          if(count($combinations) != 0) {
            $isAvailable = true;
          } else {
            $isAvailable = false;
          }
          $comb = [];
          foreach($combinations as $combination) {
            $price = 0;
            $string = '';
            foreach($combination as $ing) {
              $recipe = $cocktail->getRecipe();
              if($ing->getIsPerLiter() == true) {
                $recRow = null;
                foreach($recipe as $row) {
                  if($row['type'] == $ing->getIngredientType()->getId())
                    $recRow = $row;
                }
                $price += $ing->getPrice() * $recRow['amount'] / 100;
              } else {
                $price += $ing->getPrice();
              }

              $string = $string . $ing->getName() . ', ';
            }
            array_push(
              $comb,
              [round($price, 2), substr($string, 0, -2)],
            );
          }

          array_push(
            $cocktails,
            [
              'id' => $cocktail->getId(),
              'Name' => $cocktail->getName(),
              'Description' => $cocktail->getDescription(),
              'Recipe' => $cocktail->getRecipe(),
              'Price' => $comb,
            ]
          );
        }

        $json = $serializer->normalize($cocktails, 'json', [
          'circular_reference_handler' => function ($object) {
            return $object->getId();
          }
        ]);

        return new JsonResponse($json);
    }

    function getCombinations($options){ 
      $combinations = [[]];

      for ($count = 0; $count < count($options); $count++) {
        $tmp = [];
        foreach ($combinations as $v1) {
          foreach ($options[$count] as $v2)
            $tmp[] = array_merge($v1, [$v2]);
        }
        $combinations = $tmp;
      }

      return $combinations;
      }
}
