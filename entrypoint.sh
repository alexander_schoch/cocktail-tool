#!/usr/bin/env bash

php bin/console doctrine:migrations:migrate

chmod -R 777 /var/www/cocktails/var/cache
chmod -R 777 /var/www/cocktails/var/log

nginx
php-fpm
